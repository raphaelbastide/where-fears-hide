const d = document
const b = d.body
const map = d.querySelector('.map')
const creatures = d.querySelectorAll('.creature')
const chapters = d.querySelectorAll('.chapter')
let chapterState = 0
let creIndex = 10
const btns = d.querySelectorAll('.btn') 
const revealer = d.querySelector('.revealer')
let isInfo = isDissect = false
let panzoom, min, max, anim
let creActive
let chapterDelay = 20000
const creatureNames = ["Vegnar", "Ruh", "Balem", "Heklenaes", "Neigo", "Tunott", "Atharv", "Mulss"]
if( window.innerWidth >= 1000) {
  min = -500
  max = 500
  zoom = 1
}else{
  min = -50
  max = 50
  zoom = 3
}

window.addEventListener("load", event => {
  var image = document.querySelector('.map-bmp');
  var load = image.complete;
  image.style.display = 'block'
  map.style.background = 'none'
});
init()
function init(){
  map.querySelector('.map-bmp').style.background="none"
  creatures.forEach(creature => {
    applyTransform(creature)
    mirror(creature)
  });
  panzoom = Panzoom(map, { canvas: true, animate: true, excludeClass:'nozoom', duration:2000 })
  panzoom.zoom(2)

  setTimeout(() => panzoom.pan(min * 1, max * -3))
  const parent = map.parentElement
  parent.addEventListener('wheel', function(event) {
    panzoom.zoomWithWheel(event)
  })
  animate()
  anim = setInterval(() => {
    animate()
  }, chapterDelay);
}


function animate(){
  // chapters
  if (d.querySelector('.active')) {
    d.querySelector('.active').classList.remove('active')
  }
  let chapter = chapters[chapterState]
  creActive = chapter

  chapter.classList.add('active')
  if (chapterState >= chapters.length - 1) {
    panzoom.zoom(.5)
    panzoom.pan(100, 100, {relative:false})
    chapterState = 0
  }else{
    panzoom.zoom(zoom)
    let r1 = Math.round(Math.random() * (max - min)) + min
    let r2 = Math.round(Math.random() * (max * -4 - min * .1)) + min * .1
    panzoom.pan(r1, r2, {relative:false})
    chapterState ++
  }
  // navigation
}
function applyTransform(item){
  let els = item.querySelectorAll("div")
  let i = 0
  els.forEach(el => {
    if (!el.style.transform) {    
      el.style.setProperty('--nbr', i);
      i++
    }
  });
}
function mirror(item) {
  let left = item.querySelector('.left')
  let newright = left.cloneNode(true);
  newright.classList.remove('left')
  newright.classList.add('right')
  item.appendChild(newright)
}

function dissect(){
  creActive.style.zIndex = creIndex
  if(isDissect == true){
    clearDissect()
  }else{
    panzoom.setOptions({disablePan: true})
    panzoom.setOptions({disableZoom: true})
    revealer.focus()
    creatures.forEach(creature => {
      let els = creature.querySelectorAll('div div')
      let max = 60
      let min = -60
      els.forEach(el => {
        el.style.marginTop  = Math.round(Math.random() * (max - min)) + min + "%"
        el.style.marginLeft = Math.round(Math.random() * (max - min)) + min + "%"
      });
    });
    isDissect = true
  }
}
revealer.addEventListener('input', reveal);

function clearDissect(){
  panzoom.setOptions({disablePan: false})
  panzoom.setOptions({disableZoom: false})
  creatures.forEach(creature => {
    let els = creature.querySelectorAll('div div')
    els.forEach(el => {
      el.style.marginTop  = '0'
      el.style.marginLeft = '0'
    });
  });
  isDissect = false
}

function reveal(e){
  let val = e.target.value
  let content = val.toLowerCase()
  creatureNames.forEach(cName =>{
    cName = cName.toLowerCase()
    if(content == cName){
      let ghost = d.createElement('div')
      ghost.classList.add('ghost')
      ghost.innerHTML = val
      d.querySelector('.instructions').appendChild(ghost)
      e.target.value = ""
      document.activeElement.blur()
      setTimeout(() => {
        let creature = d.querySelector("."+cName)
        let els = creature.querySelectorAll('div')
        ghost.remove()
        els.forEach(el => {
          el.style.marginTop = "0"
          el.style.marginLeft = "0"
          el.style.transition = "transition : marginTop, marginLeft, all 5s ease-in-out;"
        })
        creature.style.zIndex = creIndex
        creIndex ++
      },1100)
    }
  }) 
}

btns.forEach(el => {
  el.onclick = function(){
    let state = this.dataset.btn
    if (d.body.dataset.state != state) {
      d.body.dataset.state = state
    }else {
      d.body.dataset.state = ""
    }
    if(state == "dissect"){dissect()}
    if(state == "next"){
      clearInterval(anim)
      animate()
      anim = setInterval(() => {
        animate()
      }, chapterDelay);
    }
    if(state == "info"){
      clearDissect()
    }
  }
})

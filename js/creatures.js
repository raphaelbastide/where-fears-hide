let d = document
let b = d.body
let started = false
let leftEls = d.querySelectorAll('main div')
let rightEls = d.querySelectorAll('.right div')
let i = j = k = 0

init()

function init(){
  applyTransform()
  mirror()
  labelDivs()
  let r1 = Math.round(Math.random() * 100)
  let r2 = Math.round(Math.random() * 100)
  b.style.backgroundPosition = `${r1}% ${r2}%`
}
function createStartButton(){
  let btn = d.createElement('div')
  btn.classList.add('start-button')
  btn.innerHTML = "tap, click or type"
  b.querySelector('main').appendChild(btn)
}
function applyTransform(){
  leftEls.forEach(el => {
    if (!el.style.transform) {    
      el.style.setProperty('--nbr', i);
      i++
    }
  });
}
function labelDivs(){
  let els = d.querySelectorAll('main div')
  els.forEach(el => {
    el.dataset.id = j
    j++
  });
}
function mirror() {
  let left = d.querySelector('.left')
  let right = d.querySelector('.right')
  if (right) {
    right.remove()
  }
  let newright = left.cloneNode(true);
  newright.classList.remove('left')
  newright.classList.add('right')
  d.querySelector('main').appendChild(newright)
}
